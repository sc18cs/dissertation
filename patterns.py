"""
Patterns in dictionary format
Each pattern reflects the structure of a phrase which
	may have sentiment about a product feature.


Patterns return a match id in the spacy depenedency matcher.
Those have been commented above each pattern

They are also used later on in order to ensure correct product
	features are extracted from the phrases based on index

	Lists of IDs passed


Match Ids are a little tricky
	patterns should be optimised and regrouped

"""



#10016797063870726367
pattern0 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "MODIFIER",
		"RIGHT_ATTRS": {"DEP": "amod"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "NEGATION",
		"RIGHT_ATTRS": {"DEP": "neg"}
	}

]
#14499690083660312615
pattern1 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "MODIFIER",
		"RIGHT_ATTRS": {"DEP": "amod"}
	}

]
#14621589392117008497
pattern2 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "NEGATION",
		"RIGHT_ATTRS": {"DEP": "neg"}
	}

]
#12054268835912785357
pattern3 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "MODIFIER",
		"RIGHT_ATTRS": {"DEP": "amod"}
	}
]
#17237321022846380202
pattern4 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},
	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "NEGATION",
		"RIGHT_ATTRS": {"DEP": "neg"}
	}

]
#8438865891019340073
pattern5 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "MODIFIER",
		"RIGHT_ATTRS": {"DEP": "amod"}
	}

]
#2402112392077543526
pattern6 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	}
]
#2402112392077543526
pattern7 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},
	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	}
]
#4176826480376464779
pattern8 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},
	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	}
]
#16703243459598810380
pattern9 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	}
]
#212452691509566318
pattern10 = [
	{
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"POS": {"IN": ["NOUN", "PROPN"]}}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	}
]
#15526379867906711492
pattern11 = [
	{
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"POS": {"IN": ["NOUN", "PROPN"]}}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "MODIFIER",
		"RIGHT_ATTRS": {"DEP": "amod"}
	}
]
#7946974431644152760
pattern12 = [
	{
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"POS": {"IN": ["NOUN", "PROPN"]}}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "MODIFIER",
		"RIGHT_ATTRS": {"DEP": "amod"}
	}
]
#9662445843388918239
pattern13 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},
	{
		"LEFT_ID": "SUBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	}
]
#8756589756371058335
pattern14 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	}
]
#9662445843388918239
pattern15 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},
{
		"LEFT_ID": "SUBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "MODIFIER",
		"RIGHT_ATTRS": {"DEP": "amod"}
	}
]

#5669508897109045523
pattern16 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "MODIFIER",
		"RIGHT_ATTRS": {"DEP": "amod"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "NEGATION",
		"RIGHT_ATTRS": {"DEP": "neg"}
	},
	{
		"LEFT_ID": "MODIFIER",
		"REL_OP": ">",
		"RIGHT_ID": "ADJECTIVE",
		"RIGHT_ATTRS": {"DEP": {"IN": ["advmod", "acomp"]}}
	}

]
#13903548378884265278
pattern17 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "MODIFIER",
		"RIGHT_ATTRS": {"DEP": "amod"}
	},
	{
		"LEFT_ID": "MODIFIER",
		"REL_OP": ">",
		"RIGHT_ID": "ADJECTIVE",
		"RIGHT_ATTRS": {"DEP": {"IN": ["advmod", "acomp"]}}
	}

]
#
pattern18 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "NEGATION",
		"RIGHT_ATTRS": {"DEP": "neg"}
	},
	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "ADJECTIVE",
		"RIGHT_ATTRS": {"DEP": {"IN": ["advmod", "acomp"]}}
	}

]
#5443602755786345329
pattern19 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "MODIFIER",
		"RIGHT_ATTRS": {"DEP": "amod"}
	},
	{
		"LEFT_ID": "MODIFIER",
		"REL_OP": ">",
		"RIGHT_ID": "ADJECTIVE",
		"RIGHT_ATTRS": {"DEP": {"IN": ["advmod", "acomp"]}}
	}
]
#
pattern20 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},

	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "NEGATION",
		"RIGHT_ATTRS": {"DEP": "neg"}
	},
	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "ADJECTIVE",
		"RIGHT_ATTRS": {"DEP": {"IN": ["advmod", "acomp"]}}
	}

]

#
pattern21 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},
	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "ADJECTIVE",
		"RIGHT_ATTRS": {"DEP": {"IN": ["advmod", "acomp"]}}
	}
]
#
pattern22 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},
	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	},
	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "ADJECTIVE",
		"RIGHT_ATTRS": {"DEP": {"IN": ["advmod", "acomp"]}}
	}
]
#
pattern23 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},
	{
		"LEFT_ID": "OBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	},
	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "ADJECTIVE",
		"RIGHT_ATTRS": {"DEP": {"IN": ["advmod", "acomp"]}}
	}
]
#
pattern24 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "OBJECT",
		"RIGHT_ATTRS": {"DEP": "dobj"}
	},
	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "ADJECTIVE",
		"RIGHT_ATTRS": {"DEP": {"IN": ["advmod", "acomp"]}}
	}
]
#
pattern25 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},
	{
		"LEFT_ID": "SUBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "COMPOUND",
		"RIGHT_ATTRS": {"DEP": "compound"}
	},
	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "ADJECTIVE",
		"RIGHT_ATTRS": {"DEP": {"IN": ["advmod", "acomp"]}}
	}
]
#
pattern26 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},
	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "ADJECTIVE",
		"RIGHT_ATTRS": {"DEP": {"IN": ["advmod", "acomp"]}}
	}
]
#
pattern27 = [
	{
		"RIGHT_ID": "VERB",
		"RIGHT_ATTRS": {"POS": {"IN": ["VERB", "AUX"]}}
	},

	{
		"LEFT_ID": "VERB",
		"REL_OP": ">",
		"RIGHT_ID": "SUBJECT",
		"RIGHT_ATTRS": {"DEP": "nsubj"}
	},
{
		"LEFT_ID": "SUBJECT",
		"REL_OP": ">",
		"RIGHT_ID": "MODIFIER",
		"RIGHT_ATTRS": {"DEP": "amod"}
	},
	{
		"LEFT_ID": "MODIFIER",
		"REL_OP": ">",
		"RIGHT_ID": "ADJECTIVE",
		"RIGHT_ATTRS": {"DEP": {"IN": ["advmod", "acomp"]}}
	}
]



patternsList = [pattern0, pattern1, pattern2, pattern3, pattern4, pattern5, pattern6, pattern7, pattern8, pattern9, pattern10, pattern11, pattern12, pattern13, pattern14, pattern15,
				pattern16, pattern17, pattern19]
#more patterns not currently in use
#pattern18, pattern19, pattern20, pattern21, pattern22, pattern23, pattern24, pattern25, pattern26, pattern27]

#patterns 0, 1, 2, 4, 7 16, 17
#add 18, 20, 22
compoundVerbSubjectPatternIds = [10016797063870726367, 14499690083660312615, 14621589392117008497, 17237321022846380202, 2402112392077543526, 5669508897109045523, 13903548378884265278]
#patterns 8, 13
#add 23, 25
noSubjectCompoundPatternIds = [4176826480376464779, 9662445843388918239]
#9 14 15
#add 24, 26, 27
noSubjectPatternIds = [16703243459598810380, 9662445843388918239]
#patterns 10, 11,
noVerbCompoundPatternIds = [15526379867906711492]
#pattern12
noVerbPatternIds = [7946974431644152760]

removaltest = [8756589756371058335,212452691509566318]
#rest not added caught in else



