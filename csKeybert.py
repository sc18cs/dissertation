from keybert import KeyBERT

bert = KeyBERT()

def csKeywordExtraction(df):
	"""

	Uses keybert to extract keywords from the review text

	:param df: pass the current version of the dataframe
	:return: returns the same df with the keywords appended in a new column
	"""
	keywordsList = []
	for i in range(0, len(df.index)):
		"""
		Bert Parameters:
		Ngram is amount of words in the keyword
		Stop words removes common words in english language
		MMR = Maximal Marginal Relevance, option to add diversity between keywords selected
		Top N is the amount of keywords it returns per review
		Diversity is a sub parameter of mmr, higher the value more diversity of terms
		"""
		keywords = bert.extract_keywords(df['reviewText'].values[i], keyphrase_ngram_range=(1, 2), stop_words="english", use_mmr=True, top_n=10, diversity=0.5)
		keywordsList.append(keywords)

	df['keywords'] = keywordsList

	return df