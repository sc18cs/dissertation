appnope @ file:///opt/concourse/worker/volumes/live/4f734db2-9ca8-4d8b-5b29-6ca15b4b4772/volume/appnope_1606859466979/work
argon2-cffi @ file:///opt/conda/conda-bld/argon2-cffi_1645000214183/work
argon2-cffi-bindings @ file:///opt/concourse/worker/volumes/live/c6f9b05d-dc80-4dbc-7473-70bfcb66883c/volume/argon2-cffi-bindings_1644569703264/work
attrs @ file:///opt/conda/conda-bld/attrs_1642510447205/work
backcall @ file:///home/ktietz/src/ci/backcall_1611930011877/work
bleach @ file:///opt/conda/conda-bld/bleach_1641577558959/work
blis==0.7.6
bpemb==0.3.3
catalogue==2.0.7
certifi==2021.10.8
cffi @ file:///private/var/folders/sy/f16zz6x50xz3113nwtb9bvq00000gp/T/croot-2p6q7e2r/cffi_1642701115063/work
charset-normalizer==2.0.12
click==8.0.4
commonmark==0.9.1
conllu==4.4.1
cycler==0.11.0
cymem==2.0.6
debugpy @ file:///opt/concourse/worker/volumes/live/32b11d06-4d64-4ec8-497a-cf4fc97343d2/volume/debugpy_1637091821874/work
decorator @ file:///opt/conda/conda-bld/decorator_1643638310831/work
defusedxml @ file:///tmp/build/80754af9/defusedxml_1615228127516/work
Deprecated==1.2.13
emoji==1.7.0
en-core-web-lg @ https://github.com/explosion/spacy-models/releases/download/en_core_web_lg-3.2.0/en_core_web_lg-3.2.0-py3-none-any.whl
en-core-web-sm @ https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-3.2.0/en_core_web_sm-3.2.0-py3-none-any.whl
entrypoints==0.3
et-xmlfile==1.1.0
filelock==3.6.0
flair==0.10
fonttools==4.31.2
ftfy==6.1.1
gdown==3.12.2
gensim==4.1.2
huggingface-hub==0.4.0
idna==3.3
importlib-metadata @ file:///opt/concourse/worker/volumes/live/4e1a3384-472f-4bcb-7776-cb0076aaea40/volume/importlib-metadata_1648562431336/work
ipykernel @ file:///opt/concourse/worker/volumes/live/c21734a4-132a-471d-7fe9-76688d5488be/volume/ipykernel_1647000841085/work/dist/ipykernel-6.9.1-py3-none-any.whl
ipython @ file:///opt/concourse/worker/volumes/live/333da4fb-3db0-439c-4a1b-ee802c44da4f/volume/ipython_1643818165055/work
ipython-genutils @ file:///tmp/build/80754af9/ipython_genutils_1606773439826/work
ipywidgets @ file:///tmp/build/80754af9/ipywidgets_1634143127070/work
Janome==0.4.2
jedi @ file:///opt/concourse/worker/volumes/live/c9d2fa99-8bc1-4572-41e7-6beba6391441/volume/jedi_1644315238822/work
jellyfish==0.9.0
Jinja2 @ file:///opt/conda/conda-bld/jinja2_1647436528585/work
joblib==1.1.0
jsonschema @ file:///Users/ktietz/demo/mc3/conda-bld/jsonschema_1630511932244/work
jupyter==1.0.0
jupyter-client @ file:///opt/conda/conda-bld/jupyter_client_1643638337975/work
jupyter-console @ file:///opt/conda/conda-bld/jupyter_console_1647002188872/work
jupyter-core @ file:///opt/concourse/worker/volumes/live/b49dfe5a-aa7f-4da7-60c3-fdd95ee78394/volume/jupyter_core_1646994453026/work
jupyterlab-pygments @ file:///tmp/build/80754af9/jupyterlab_pygments_1601490720602/work
jupyterlab-widgets @ file:///tmp/build/80754af9/jupyterlab_widgets_1609884341231/work
keybert==0.5.0
kiwisolver==1.4.2
konoha==4.6.5
langcodes==3.3.0
langdetect==1.0.9
lxml==4.8.0
MarkupSafe==2.1.1
matplotlib==3.5.1
matplotlib-inline @ file:///tmp/build/80754af9/matplotlib-inline_1628242447089/work
mistune==0.8.4
more-itertools==8.8.0
mpld3==0.3
murmurhash==1.0.6
nbclient @ file:///tmp/build/80754af9/nbclient_1645431659072/work
nbconvert @ file:///private/var/folders/sy/f16zz6x50xz3113nwtb9bvq00000gp/T/croot-r4wh23j5/nbconvert_1641309205559/work
nbformat @ file:///tmp/build/80754af9/nbformat_1617383369282/work
nest-asyncio @ file:///tmp/build/80754af9/nest-asyncio_1613680548246/work
networkx==2.6.3
nltk==3.7
notebook @ file:///opt/concourse/worker/volumes/live/cd460127-b2af-410b-4512-5d8138402374/volume/notebook_1645002559399/work
numpy==1.21.5
openpyxl==3.0.9
overrides==3.1.0
packaging @ file:///tmp/build/80754af9/packaging_1637314298585/work
pandas==1.3.5
pandocfilters @ file:///opt/conda/conda-bld/pandocfilters_1643405455980/work
parso @ file:///opt/conda/conda-bld/parso_1641458642106/work
pathy==0.6.1
pexpect @ file:///tmp/build/80754af9/pexpect_1605563209008/work
pickleshare @ file:///tmp/build/80754af9/pickleshare_1606932040724/work
Pillow==9.0.1
preshed==3.0.6
prometheus-client @ file:///opt/conda/conda-bld/prometheus_client_1643788673601/work
prompt-toolkit @ file:///tmp/build/80754af9/prompt-toolkit_1633440160888/work
protobuf==3.19.4
ptyprocess @ file:///tmp/build/80754af9/ptyprocess_1609355006118/work/dist/ptyprocess-0.7.0-py2.py3-none-any.whl
pycparser @ file:///tmp/build/80754af9/pycparser_1636541352034/work
pydantic==1.8.2
Pygments @ file:///opt/conda/conda-bld/pygments_1644249106324/work
pyparsing==3.0.7
pyrsistent @ file:///opt/concourse/worker/volumes/live/24b7a9ab-37d8-463c-575f-69184f9cfbc8/volume/pyrsistent_1636111022304/work
PySocks==1.7.1
python-dateutil @ file:///tmp/build/80754af9/python-dateutil_1626374649649/work
pytz==2021.3
PyYAML==6.0
pyzmq @ file:///opt/concourse/worker/volumes/live/7dec6841-64b1-4d95-7226-ce37c6ca8915/volume/pyzmq_1638435000749/work
qtconsole @ file:///opt/conda/conda-bld/qtconsole_1649078897110/work
QtPy @ file:///opt/conda/conda-bld/qtpy_1649073884068/work
rake-nltk==1.0.6
regex==2022.3.15
requests==2.27.1
rich==12.0.1
sacremoses==0.0.49
scikit-learn==1.0.2
scipy==1.7.3
seaborn==0.11.2
segtok==1.5.11
Send2Trash @ file:///tmp/build/80754af9/send2trash_1632406701022/work
sentence-transformers==2.2.0
sentencepiece==0.1.95
six @ file:///tmp/build/80754af9/six_1644875935023/work
smart-open==5.2.1
spacy==3.2.3
spacy-legacy==3.0.9
spacy-loggers==1.0.1
sqlitedict==2.0.0
srsly==2.4.2
stanfordnlp==0.2.0
stanza==1.3.0
tabulate==0.8.9
terminado @ file:///opt/concourse/worker/volumes/live/9f5c2831-bf90-4436-71cc-89a70d3fece2/volume/terminado_1644322615956/work
testpath @ file:///tmp/build/80754af9/testpath_1624638946665/work
thinc==8.0.15
threadpoolctl==3.1.0
tokenizers==0.11.6
torch==1.11.0
torchvision==0.12.0
tornado @ file:///opt/concourse/worker/volumes/live/d531d395-893c-4ca1-6a5f-717b318eb08c/volume/tornado_1606942307627/work
tqdm==4.63.0
traitlets @ file:///tmp/build/80754af9/traitlets_1636710298902/work
transformers==4.17.0
typer==0.4.0
typing_extensions @ file:///opt/conda/conda-bld/typing_extensions_1647553014482/work
urllib3==1.26.9
wasabi==0.9.0
wcwidth @ file:///Users/ktietz/demo/mc3/conda-bld/wcwidth_1629357192024/work
webencodings==0.5.1
widgetsnbextension @ file:///opt/concourse/worker/volumes/live/d0e5739b-8078-4a19-69bb-be63aeed72df/volume/widgetsnbextension_1645009381873/work
Wikipedia-API==0.5.4
wordcloud==1.8.1
wrapt==1.14.0
yake==0.4.8
zipp @ file:///opt/conda/conda-bld/zipp_1641824620731/work
