import pandas as pd
import time


def cleanse(filepath):
	"""
	Opens json into a dataframe and drops all but required columns.
	Timer for opening files.

	:param filepath: The full filepath of the json file to be cleansed
	:return: returns a dataframe with only the asin (product number), reviewtext and overall (the rating out of 5) columns
	"""
	startTime = time.time()
	print("BIG TEST Read Started at: ", time.time())
	df = pd.read_json(filepath)
	print("BIG TEST Read Finished at: ", time.time())
	print("BIG TEST Read took: ", time.time() - startTime)

	#dropped fields
	cleansed_df = df[['asin','reviewText','overall']]

	return cleansed_df

