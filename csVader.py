from nltk.sentiment import SentimentIntensityAnalyzer


def csSentiment(df):
	"""

	Takes the phrase from the dependency df
	Conducts sentiment analysis on the phrase

	:param df: current version of df
	:return: df with sentiment scores appended
	"""

	#initialise classifier
	classifier = SentimentIntensityAnalyzer()
	scoredKeywordsColumn = []
	for i in range(0,len(df.index)):
		scoredKeywords = []
		keywords = df['Keywords and Descriptions'].values[i]
		for j in range(0, len(keywords)):
			keyword = keywords[j]
			descriptionList = keyword[1]
			#combining the words into a phrase for sentiment
			description = ' '.join(map(str, descriptionList))
			sentiment_dictionary = classifier.polarity_scores(description)
			sentimentScore = sentiment_dictionary.get('compound')
			keyword.append(sentimentScore)
			scoredKeywords.append(keyword)
		scoredKeywordsColumn.append(scoredKeywords)
	df["scoredKeywords"] = scoredKeywordsColumn

	return df
