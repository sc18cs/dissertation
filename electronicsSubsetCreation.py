import json
import pandas as pd
import numpy as np
import re
from pathlib import Path
import time
import numpy


def removeBooks(startTime):
	"""

	:param startTime: passed start time to calculate run time (old version)
	:return: no return just writes to new smaller file
	"""
	electronics_df = pd.read_json("Data/Electronics_5.json", lines=True)
	timeToRead = time.time()-startTime
	print("Read Time: ", timeToRead)


	electronics_df.info(verbose=False)

	kindleVariable = {"Format:": " Kindle Edition"}
	hardcoverVariable = {"Format:": " Hardcover"}
	paperbackVariable = {"Format:": " Paperback"}
	electronicsNoKindle_df = electronics_df.loc[electronics_df['style'] != kindleVariable]
	electronicsNoHardcover_df = electronicsNoKindle_df.loc[electronics_df['style'] != hardcoverVariable]
	electronicsNoBooks_df = electronicsNoHardcover_df.loc[electronics_df['style'] != paperbackVariable]


	startTime = time.time()
	electronicsNoBooks_df.to_json(r'/Users/conor/Desktop/Local Dissertation/PyCharm/Data/ElectronicsNoBooks.json', orient="records")
	timeToWrite = time.time() - startTime
	print("Write Time: ", timeToWrite)



def Sample(rows):
	"""

		:param rows: samples the file taking the number of rows passed
		:return: no return just writes to new smaller file
		"""
	print("Sample has started running")
	startTime = time.time()
	electronics_df = pd.read_json("Data/ElectronicsNoBooks.json", orient="records")
	timeToRead = time.time() - startTime
	print("Read Time: ", timeToRead)

	electronics_df = electronics_df.sample(n=rows, random_state=25, replace=True)

	startTime = time.time()
	electronics_df.to_json(r'/Users/conor/Desktop/Local Dissertation/PyCharm/Data/Electronics100.json', orient="records")
	timeToWrite = time.time() - startTime
	print("Write Time: ", timeToWrite)



def cameraExample():
	"""
	Creates a file with only the asins hard coded in which were manually selected as cameras

	:return: writes to smaller file
	"""
	print("Sample has started running")
	startTime = time.time()
	electronics_df = pd.read_json("Data/ElectronicsNoBooks.json", orient="records")
	timeToRead = time.time() - startTime
	print("Read Time: ", timeToRead)

	electronics_df = electronics_df.loc[electronics_df['asin'].isin(["B0012Y6HC8", "B0012YA85A", "B0011ZCDKS", "B0011ZK6PC", "B00I8BICCG"])]

	startTime = time.time()
	electronics_df.to_json(r'/Users/conor/Desktop/Local Dissertation/PyCharm/Data/CameraExample.json', orient="records")
	timeToWrite = time.time() - startTime
	print("Write Time: ", timeToWrite)

def singleCameraExample():
	"""
	Creates a file with only the asin hard coded in which were manually selected as one of the cameras

	:return: writes to smaller file
	"""
	print("Sample has started running")
	startTime = time.time()
	electronics_df = pd.read_json("Data/CameraExample.json", orient="records")
	timeToRead = time.time() - startTime
	print("Read Time: ", timeToRead)

	electronics_df = electronics_df.loc[electronics_df['asin'].isin(["B0012Y6HC8"])]

	startTime = time.time()
	electronics_df.to_json(r'/Users/conor/Desktop/Local Dissertation/PyCharm/Data/SingleCameraExample.json', orient="records")
	timeToWrite = time.time() - startTime
	print("Write Time: ", timeToWrite)



def investigateElectronics():
	"""
	Used for finding which asins might be cameras, which were there verified manually

	:return: writes to summary file
	"""
	print("Sample has started running")
	startTime = time.time()
	electronics_df = pd.read_json("Data/ElectronicsNoBooks.json", orient="records")
	timeToRead = time.time() - startTime
	print("Read Time: ", timeToRead)

	cleansed_df = electronics_df[['asin', 'reviewText', 'overall']]

	cameras = cleansed_df['reviewText'].str.contains("camera", case=False, na=False)
	cleansed_df = cleansed_df[cameras]

	grouped_df = cleansed_df.groupby(['asin'], as_index=False, sort=True).agg(
		mean=pd.NamedAgg(column='overall', aggfunc=numpy.mean),
		count=pd.NamedAgg(column='overall', aggfunc='count'), )

	grouped_df = grouped_df.sort_values(['count', 'mean', 'asin'], ascending=[False, False, True])

	startTime = time.time()
	grouped_df.to_json('/Users/conor/Desktop/Local Dissertation/PyCharm/Data/cameraAsinSummary.json', orient="records")
	timeToWrite = time.time() - startTime
	print("Write Time: ", timeToWrite)



startTime = time.time()

"""
Commands that can be run
"""

#removeBooks(startTime)
#Sample(100)
#cameraExample()
#singleCameraExample()
#investigateElectronics()
