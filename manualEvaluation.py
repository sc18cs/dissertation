from dataCleansing import cleanse
from csSpacy import csDependencyMatching
from csKeybert import csKeywordExtraction
from csVader import csSentiment
import time

print("MANUAL TESTING")
ProgramStartTime = time.time()
df = cleanse("Data/nonPositiveManualTestingSample.json")

keyBert_df = csKeywordExtraction(df)

dependency_df = csDependencyMatching(keyBert_df)

#Compares the keywords from keybert and the candidates from the dependency matcher
keywordSubsetColumn = []
for i in range(0,len(dependency_df.index)):
	keywordSubset = []
	for j in range(0,len(dependency_df['keywords'].values[i])):
		for k in range(0, len(dependency_df['Keywords and Descriptions'].values[i])):
			if dependency_df['keywords'].values[i][j][0] == dependency_df['Keywords and Descriptions'].values[i][k][0]:
				keywordSubset.append(dependency_df['Keywords and Descriptions'].values[i][k])
	keywordSubsetColumn.append(keywordSubset)
#Uses keywords subset to narrow down the potential keywords from dependency  matcher

dependency_df['Keywords and Descriptions'] = keywordSubsetColumn

vaderSentiment_df = csSentiment(dependency_df)

for i in range(0,len(vaderSentiment_df.index)):
	scoredKeywords = vaderSentiment_df["scoredKeywords"].values[i]
	for j in range(0, len(scoredKeywords)):
		print("Review: ")
		print(vaderSentiment_df['reviewText'].values[i])
		print("KEYWORD: ", scoredKeywords[j][0], " Score : ", scoredKeywords[j][2])



print("WHOLE TEST TOOK: ", time.time() - ProgramStartTime)
