#AND SO IT ENDS

from dataCleansing import cleanse
from csSpacy import csDependencyMatching
from csKeybert import csKeywordExtraction
from csVader import csSentiment
import pandas as pd
import numpy
import time

print("AND SO BEGINS THE BEGINNING OF THE END V1.0")
ProgramStartTime = time.time()
startTime = time.time()
print("BIG TEST Cleanse Started at: ", time.time())
df = cleanse("Data/CameraExample.json")
print("BIG TEST Cleanse Finished at: ", time.time())
print("BIG TEST Cleanse took: ", time.time() - startTime)


startTime = time.time()
print("BIG TEST Keybert Started at: ", time.time())
keyBert_df = csKeywordExtraction(df)
print("BIG TEST Keybert Finished at: ", time.time())
print("BIG TEST Keybert took: ", time.time() - startTime)


startTime = time.time()
print("BIG TEST Dependency Match Started at: ", time.time())
dependency_df = csDependencyMatching(keyBert_df)
print("BIG TEST Dependency Match Finished at: ", time.time())
print("BIG TEST Dependency Match took: ", time.time() - startTime)

#Compares the keywords from keybert and the candidates from the dependency matcher
keywordSubsetColumn = []
for i in range(0,len(dependency_df.index)):
	keywordSubset = []
	for j in range(0,len(dependency_df['keywords'].values[i])):
		for k in range(0, len(dependency_df['Keywords and Descriptions'].values[i])):
			if dependency_df['keywords'].values[i][j][0] == dependency_df['Keywords and Descriptions'].values[i][k][0]:
				keywordSubset.append(dependency_df['Keywords and Descriptions'].values[i][k])
	keywordSubsetColumn.append(keywordSubset)


#Uses keywords subset to narrow down the potential keywords from dependency  matcher

dependency_df['Keywords and Descriptions'] = keywordSubsetColumn



startTime = time.time()
print("BIG TEST VADER Started at: ", time.time())
vaderSentiment_df = csSentiment(dependency_df)
print("BIG TEST VADER Finished at: ", time.time())
print("BIG TEST VADER took: ", time.time() - startTime)



"""

Final Cleanse
Restructures the dataframe into the best format for the visualisations

"""


startTime = time.time()
print("BIG TEST Final Cleanse Started at: ", time.time())
cleansedVader_df = vaderSentiment_df.drop('reviewText', 1)
cleansedVader_df = cleansedVader_df.drop('overall', 1)
cleansedVader_df = cleansedVader_df.drop('Keywords and Descriptions', 1)

asinList = []
featureList = []
scoreList = []
for i in range(0,len(cleansedVader_df.index)):
	scoredKeywords = cleansedVader_df["scoredKeywords"].values[i]
	for j in range(0,len(scoredKeywords)):
		asinList.append(cleansedVader_df["asin"].values[i])
		featureList.append(scoredKeywords[j][0])
		scoreList.append(scoredKeywords[j][2])

featureLevel_df = pd.DataFrame(numpy.column_stack([asinList, featureList, scoreList]), columns=['asin', 'feature', 'score'])

featureLevel_df['score'] = pd.to_numeric(featureLevel_df['score'], downcast="float")

groupedFeature_df = featureLevel_df.groupby(['asin', 'feature'], as_index=False, sort=True).agg(mean=pd.NamedAgg(column='score', aggfunc=numpy.mean),
																	 count=pd.NamedAgg(column='score', aggfunc='count'),)

groupedFeature_df = groupedFeature_df.sort_values(['asin','count', 'mean', 'feature'], ascending=[True, False, False, True])

print("BIG TEST Final Cleanse Finished at: ", time.time())
print("BIG TEST Final Cleanse took: ", time.time() - startTime)


print(groupedFeature_df)
groupedFeature_df.to_json('/Users/conor/Desktop/Local Dissertation/PyCharm/Data/camerasOutput.json', orient="records")

print("WHOLE TEST TOOK: ", time.time() - ProgramStartTime)






