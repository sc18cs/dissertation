import spacy
from spacy.matcher import DependencyMatcher
from patterns import patternsList, compoundVerbSubjectPatternIds, noSubjectCompoundPatternIds, noSubjectPatternIds, noVerbCompoundPatternIds, noVerbPatternIds, removaltest

def csDependencyMatching(df):
	"""
	Uses the spacy dependency tree structure in order to pull out potential product features along with their dependent words.
		Phrases are selected if they match any of the patterns brought in from patterns.py

	:param df: pass the current version of the dataframe
	:return: returns the same df with the candidate product features and their associated words appended in a new column
	"""

	nlp = spacy.load("en_core_web_lg")

	allReviewMatches = []

	#initialises the dependency matcher and adds all the patterns from the full list
	matcher = DependencyMatcher(nlp.vocab)
	for i in range(0, len(patternsList)):
		name = "Pattern" + str(i)
		matcher.add(name, [patternsList[i]])

	for i in range(0,len(df.index)):
		review = nlp(df['reviewText'].values[i])
		matches = matcher(review)
		reviewMatches = []
		for j in range(0, len(matches)):
			#cycle through the matches
			words = []
			object = []
			fullMatch = []
			matchTypeId, wordIds = matches[j]
			#checks which category match type is to pick out correct object(s) words
			#takes the lemma to help remove duplication
			if matchTypeId in compoundVerbSubjectPatternIds:
				object = review[wordIds[3]].lemma_ + " " + review[wordIds[2]].lemma_
			elif matchTypeId in noSubjectCompoundPatternIds:
				object = review[wordIds[2]].lemma_ + " " + review[wordIds[1]].lemma_
			elif matchTypeId in noSubjectPatternIds:
				object = review[wordIds[1]].lemma_
			elif matchTypeId in noVerbCompoundPatternIds:
				object = review[wordIds[1]].lemma_ + " " + review[wordIds[0]].lemma_
			elif matchTypeId in noVerbPatternIds:
				object = review[wordIds[0]].lemma_
			elif matchTypeId in removaltest:
				continue
			else:
				object = review[wordIds[2]].lemma_
			#puts words back in order of sentence
			wordIds.sort()
			for k in range(len(wordIds)):
				word = review[wordIds[k]].text
				words.append(word)
			fullMatch = [object, words]
			if fullMatch not in reviewMatches:
				reviewMatches.append(fullMatch)
		popList = []
		#checks if a subset phrase has been picked up and makes sure to only keep the most detailed version of the phrase
		for j in range(0, len(reviewMatches)):
			for k in range(0, len(reviewMatches)):
				if j != k:
					if set(reviewMatches[j][1]).issubset(set(reviewMatches[k][1])):
						if reviewMatches[j][1] not in popList:
							popList.append(reviewMatches[j][1])
					elif set(reviewMatches[k][1]).issubset(set(reviewMatches[j][1])):
						if reviewMatches[k][1] not in popList:
							popList.append(reviewMatches[k][1])

		#removes all the subsets that have been stored in the poplist
		for i in range(0, len(reviewMatches)):
			for j in range(0, len(popList)):
				try:
					if reviewMatches[i][1] == popList[j]:
						reviewMatches.pop(i)
				except IndexError:
					continue

		allReviewMatches.append(reviewMatches)

	df['Keywords and Descriptions'] = allReviewMatches

	return df


