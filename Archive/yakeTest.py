import yake

def csYake(df):

	language = "en"
	max_ngram_size = 2
	#keywords per review
	kpr = 3
	customer_kw_extractor = yake.KeywordExtractor(lan=language, n=max_ngram_size, top=kpr, features=None)
	keywordsList = []
	for i in range(0, len(df.index)):
		keywords = customer_kw_extractor.extract_keywords(df['reviewText'].values[i])
		keywordsList.append(keywords)
	df['keywords'] = keywordsList

	return df