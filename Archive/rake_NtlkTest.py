from rake_nltk import Rake


def csRake(df):
	rake_kw_extractor = Rake(min_length=1, max_length=2)

	keywordsList = []
	for i in range(0,len(df.index)):
		rake_kw_extractor.extract_keywords_from_text(df['reviewText'].values[i])
		keywords = rake_kw_extractor.get_ranked_phrases()
		keywordsList.append(keywords)
	df['keywords'] = keywordsList

	return df