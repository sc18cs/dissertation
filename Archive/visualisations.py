from wordcloud import WordCloud, ImageColorGenerator
from PIL import Image
import matplotlib.pyplot as plt

import pandas as pd
import numpy as np


def colourFunction(word, font_size, position, orientation, random_state=None, **kwargs):
	return "hsl(0, 0%%, %d%%)" % 60


def csWordCloud(df, stopwords, title, shape=None):
	print(df.columns)
	wcData = df.drop(['asin','mean'], axis=1)
	print(wcData)


	#CAN STOPWORDS BE AUTOMATED

	#color_func = colourFunction(df),

	wc = WordCloud(scale=4,
				   max_words=150,
				   colormap='bwr',
				   mask=shape,
				   background_color='white',
				   stopwords=stopwords,
				   collocations=True).generate_from_frequencies(wcData)
	plt.figure(figsize=(10,8))
	plt.imshow(wc.recolor(color_func=colourFunction, random_state=3), interpolation="bilinear")
	plt.axis('off')
	plt.title(title, fontsize=13)
	plt.show()

stopwords = ['camera', 'cameras', 'photos', 'photo', 'pictures', 'picture']
camera_shape = np.array(Image.open('/Users/conor/Desktop/Local Dissertation/PyCharm/images/genericCamera.jpeg'))

df = pd.read_json("Data/camerasOutput.json", orient="records")
asinList = ["B0012Y6HC8", "B0012YA85A", "B0011ZCDKS", "B0011ZK6PC", "B00I8BICCG"]
for asin in asinList:
	title = asin + " Wordcloud"
	df = df.loc[df['asin'].isin([asin])]
	csWordCloud(df, stopwords, title)