from flair.embeddings import FlairEmbeddings, BertEmbeddings, WordEmbeddings, DocumentRNNEmbeddings, StackedEmbeddings
from flair.data import Sentence
from flair.models import TextClassifier
import time
"""

Take [Keywords + words either side] as a 'sentence'

Run sentiment analysis on it

Each 'sentence' must only have one keyword.

Check if flair account for Negation


"""
"""
flair_forward = FlairEmbeddings('multi-forward-fast')
flair_backwards = FlairEmbeddings('multi-backward-fast')

bert_additional = BertEmbeddings('bert-base-cased')

stack = StackedEmbeddings(embeddings=[flair_forward, flair_backwards, bert_additional])
"""

def csFlairSentiment(df):
	startTime = time.time()
	for i in range(0,1):
		print(df['reviewText'].values[i])
		keywordPair = df['Keywords and Descriptions'].values[i]
		for j in range(0, len(keywordPair)):
			descriptionList = keywordPair[j][1]
			description = ' '.join(map(str, descriptionList))
			#description = str(description)
			print("Description ", description)
			classifier = TextClassifier.load('en-sentiment')
			sentence = Sentence(description)
			classifier.predict(sentence)
			print(sentence.labels)
			keywordPair[j].append(sentence.labels)
			print(keywordPair[j])
	sentimentTime = time.time() - startTime
	print("FLAIR SENTIMENT TIME :", sentimentTime)

