import spacy
import pandas as pd
from spacy.matcher import DependencyMatcher
from patterns import patternsList, compoundVerbSubjectPatternIds, noSubjectCompoundPatternIds, noSubjectPatternIds, noVerbCompoundPatternIds, noVerbPatternIds
from spacy import displacy

reviews = "The meal we had yesterday was lovely. My trip to Belfast was very nice. I found the meal to be tasty. The service was slow."

nlp = spacy.load("en_core_web_lg")

review = nlp(reviews)
sentences = list(review.sents)
displacy.serve(sentences, style="dep")


