import spacy
import pandas as pd
from spacy.matcher import DependencyMatcher
from patterns import patternsList, compoundVerbSubjectPatternIds, noSubjectCompoundPatternIds, noSubjectPatternIds, noVerbCompoundPatternIds, noVerbPatternIds, removaltest


def csSpacyKW(df):

	nlp = spacy.load("en_core_web_lg")
	globalKeywordsList = []
	for i in range(0,len(df.index)):
		reviewKeywordsList = []
		keywords = nlp(df['reviewText'].values[i]).ents
		for keyword in keywords:
			reviewKeywordsList.append(keyword.text)
		globalKeywordsList.append(reviewKeywordsList)
	df['keywords'] = globalKeywordsList

	return df


def csSpacyPOS(df):
	nlp = spacy.load("en_core_web_lg")

	nounsList =[]
	adjectivesList = []
	for i in range(0,len(df.index)):
		review = nlp(df['reviewText'].values[i])
		"""
		
		Go through all words
		Find a noun
		Check if it's a compound noun
			It's a compound noun if
				
		
		Types of words I need
		Nouns/ProperNouns (Compound and not) - NOUN, PROPN
		Adjectives - ADJ
		Adverbs - ADV
		Particles (Only Not and negators) - PART
		Verb - VERB
		
		IF CONJ on nouns then apply all the same
		
		START WITH VERB - RECURSIVE
		
		Structure should be Noun - Sentence - Then sentiment score the sentence
		
		LOOK AT DEPENDENCY MATCHER SPACY
		
		"""

		"""for word in review:
			csChunk = []
			if word.pos_ in ["NOUN", "PROPN"]:
				if word.dep_ != "compound":
					for child in word.children:
						if child.dep == "compound":
							csCompoundNoun = word + " " + child
							csChunk.append(csCompoundNoun)
						else:
							csChunk.append(word)


		"""

		nouns = [word.lemma_.lower().strip() for word in review if word.pos_ in ["NOUN", "PROPN"]]
		nounsList.append(nouns)
		adjectives = [word.lemma_.lower().strip() for word in review if word.pos_ == "ADJ"]
		adjectivesList.append(adjectives)

	df['reviewNouns'] = pd.Series(nounsList)
	df['reviewAdjectives'] = pd.Series(adjectivesList)

	return df

def csSpacySKW(KW_df, POS_df):
	globalSuitableKeywords =[]
	for i in range(0, len(KW_df.index)):
		reviewSuitableKeywords = []
		for word in KW_df.iloc[i]['keywords']:
			for noun in POS_df.iloc[i]['reviewNouns']:
				#print("COMPARE: ", noun, " AND ", word)
				if noun == word:
					reviewSuitableKeywords.append(noun)
		globalSuitableKeywords.append(reviewSuitableKeywords)
	KW_df['suitableKeywords'] = globalSuitableKeywords

	return KW_df


def csDependencyMatching(df):
	nlp = spacy.load("en_core_web_lg")

	allReviewMatches = []
	matcher = DependencyMatcher(nlp.vocab)
	for i in range(0, len(patternsList)):
		name = "Pattern" + str(i)
		matcher.add(name, [patternsList[i]])

	for i in range(0,len(df.index)):
		review = nlp(df['reviewText'].values[i])
		matches = matcher(review)
		reviewMatches = []
		for j in range(0, len(matches)):
			#cycle through the matches
			words = []
			object = []
			fullMatch = []
			matchTypeId, wordIds = matches[j]
			if matchTypeId in compoundVerbSubjectPatternIds:
				object = review[wordIds[3]].lemma_ + " " + review[wordIds[2]].lemma_
			elif matchTypeId in noSubjectCompoundPatternIds:
				object = review[wordIds[2]].lemma_ + " " + review[wordIds[1]].lemma_
			elif matchTypeId in noSubjectPatternIds:
				object = review[wordIds[1]].lemma_
			elif matchTypeId in noVerbCompoundPatternIds:
				object = review[wordIds[1]].lemma_ + " " + review[wordIds[0]].lemma_
			elif matchTypeId in noVerbPatternIds:
				object = review[wordIds[0]].lemma_
			elif matchTypeId in removaltest:
				continue
			else:
				object = review[wordIds[2]].lemma_
			wordIds.sort()
			for k in range(len(wordIds)):
				word = review[wordIds[k]].text
				words.append(word)
			fullMatch = [object, words]
			if fullMatch not in reviewMatches:
				reviewMatches.append(fullMatch)
		popList = []
		for j in range(0, len(reviewMatches)):
			for k in range(0, len(reviewMatches)):
				if j != k:
					if set(reviewMatches[j][1]).issubset(set(reviewMatches[k][1])):
						if reviewMatches[j][1] not in popList:
							popList.append(reviewMatches[j][1])
					elif set(reviewMatches[k][1]).issubset(set(reviewMatches[j][1])):
						if reviewMatches[k][1] not in popList:
							popList.append(reviewMatches[k][1])

		for i in range(0, len(reviewMatches)):
			for j in range(0, len(popList)):
				try:
					if reviewMatches[i][1] == popList[j]:
						reviewMatches.pop(i)
				except IndexError:
					continue

		allReviewMatches.append(reviewMatches)

	df['Keywords and Descriptions'] = allReviewMatches

	return df


"""	
	Find new match type keys
	for j in range(0, len(matches)):
			matchTypeId, wordIds = matches[j]
			words = []
			for k in range(len(wordIds)):
				word = review[wordIds[k]].text
				words.append(word)
			print("MATCH TYPE: ", matchTypeId, " Words ", words)
"""

