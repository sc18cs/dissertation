import pandas as pd

#read from file
df = pd.read_excel('Data/Local Survey Responses 13th April.xlsm', sheet_name="Anonymised Responses")

#group by id columns and transpose rest
transposed_df = pd.melt(df,
              id_vars=['USER ID', "Chart Type"],
              value_vars=df.columns[1:])

#write to file
transposed_df.to_excel('Data/CleansedSurvey.xlsx', sheet_name='CleansedResponses')

